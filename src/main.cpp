#include "noname/network/network.hpp"

using namespace noname_core::network;

int main(int argc, char** argv)
{
	if (argc != 3) return -1;

	char* s = argv[1];
	int s_len = strlen(s);
	char* d = argv[2];
	int d_len = strlen(d);

	WindivertNetworkAdaper adaper;
	tcpflowmanager manager(
		adaper, 
		[&](packet p) {
			byte* data = p.data();
			int change = 0;

			for (int i = 0; i <= p.size() - s_len; ++i) {
				if (!memcmp(data + i, s, s_len)) {
					
					//change
					if (d_len - s_len == 0) {
						memcpy(data + i, d, d_len);
					}
					else if (d_len > s_len) {
						int sub = d_len - s_len;
						for (int j = 0; j < sub; ++j)
							p.insert(p.begin() + i, 0);
						memcpy(data + i, d, d_len);
						change += sub;
					}
					else {
						int sub = s_len - d_len;
						for (int j = 0; j < sub; ++j)
							p.erase(p.begin() + i);
						memcpy(data + i, d, d_len);
						change -= sub;
					}
				}
			}
			return change;
		}
	);

	manager.start();
	manager.loop();

	return 0;
}